<!DOCTYPE html>
<html>

<head>
    <title>Découvrir AlplineJS</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <script src="https://cdn.tailwindcss.com"></script>
    <style type="text/css" href="app.css"></style>
</head>

<body class="bg-gray-200">
{{View::make('./layouts/header')}}
@yield('content')
{{View::make('./layouts/footer')}}

        <!--<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>-->
        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>


<script type="module" src="new.js"></script>
</body>

</html>