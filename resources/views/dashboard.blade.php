@extends('master')
@section("content")
    <main class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 my-6 md:my-12">

        <h1 class="text-2xl font-semibold text-gray-900 mb-4">Users list</h1>

        <div class="relative rounded-md shadow-sm py-2 max-w-xs">
            <input type="text" name="search" id="search" class="pl-2 py-2 focus:ring-indigo-500 focus:border-indigo-500 block w-full pr-10 sm:text-sm border-gray-300 rounded-md" autocomplete="off" placeholder="Search">
            <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none text-gray-500">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
		      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
		    </svg>
            </div>
        </div>

        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table id="users-table" class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        <div class="flex items-center">
                                            <a id="sortByName" href="#_">Name</a>
                                        </div>
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        <div class="flex items-center">
                                            <a href="#_">Address</a>
                                        </div>
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        <div class="flex items-center">
                                            <a href="#_">Website</a>
                                        </div>
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        <div class="flex items-center">
                                            <a href="#_">Phone</a>
                                        </div>
                                    </th>
                                    <th scope="col" class="relative px-6 py-3">
                                        <span class="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200" x-data="{users:[]}" x-init="fetch('https://jsonplaceholder.typicode.com/users')
                                   .then(response => response.json())
                                   .then(data => users = data)">
                                <!-- Users -->
                                <!-- :key="user.id" -->

                                <template x-for="user in users">
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">
                                                <div class="flex-shrink-0 h-10 w-10">
                                                    <img class="h-10 w-10 rounded-full" :src="`https://i.pravatar.cc/300`+user.id" alt="">
                                                </div>
                                                <div class="ml-4">
                                                    <div x-text="user.name" class="text-sm font-medium text-gray-900"></div>
                                                    <div x-bind:href="'mailto:' + user.email" x-text="user.email" class="text-sm text-gray-500"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div x-text="user.address.street" class="text-sm text-gray-900"></div>
                                            <div x-text="user.address.zipcode"class="text-sm text-gray-500"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <span x-text="user.website" class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                           
                              </span>
                                        </td>
                                        <td x-text="user.phone" class="px-6 py-4 whitespace-nowrap text-sm text-gray-500"></td>
                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                                        </td>
                                    </tr>
                                </template>

                                <!-- More people... -->

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @endsection

